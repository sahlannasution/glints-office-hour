// Write if-else statements to fulfill the following conditions:
// if score morethan 60 you pass otherwise you failed

const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
})


function isEmptyOrSpaces(str){
  return str === null || str.match(/^ *$/) !== null;
}

function testScore(num) {
  if (num <= 60) {
    return `I'm so sorry you failed the test! Your score is ${num}\n`
    
  } else {
    return `Hooray! You have passed the test! Your score is ${num}\n`
  }

}

function inputNilai() {
  rl.question(`Masukkan Nilai: `, nilai => {
    if (!isNaN(nilai) && !isEmptyOrSpaces(nilai)) {
      console.log(testScore(nilai));
      rl.close()
    } else {
      console.log(`Nilai harus berupa angka dan tidak boleh kosong!\n`);
      inputNilai()
    }
  })
}

// console.log(testScore(50));
// console.log(testScore(90));
inputNilai()
//testScore(50) should return "Failed"
//testScore(90) should return "Passed"

// Instruction
// Create a function to multiply the inputs
// Your code should validate the input only accept the NUMBER type and cannot BLANK

function multiply(num1, num2) {
    if (typeof num1 === undefined || typeof num2 === undefined) {
        return `Please provide a number only!`
    } else if (typeof num1 === 'string' || typeof num2 === 'string' || typeof num1 === "boolean" || typeof num2 === "boolean" || num1 === null || num2 === null) {
        return `Please provide a value!`
    } else {
        result = num1 * num2
        return `The answer is ${result}`
    }
}
//example 1
// num1 = 2
// num2 = 4

//example 2
// num1 = "2"
// num2 = 4
//example 3
num1 = true
num2 = null
//example 4
// num1 = undefined
// num2 = "2"

console.log(multiply(num1, num2))

// example
// num1 = 2, num2 = 4, return 8
// num1 = "2", num2 = 4, return "Please provide a number only!"
// num1 = true, num2 = null, return "Please provide a number only!"
// num1 UNDEFINED, num2 = "2", return "Please provide a value!"
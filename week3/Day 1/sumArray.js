//Declare and initialize a variable total to 0. Use a for loop to add the value of each element of the myArr array to total.


var myArr = [ 2, 3, 4, 5, 6];

// Only change code below this line
function sumArray(arr) {
    return arr.reduce((a, b) => a + b, 0)
}

//total should equal 20.
console.log(`The Array : ${myArr}\nThe SUM of myArr array : ${sumArray(myArr)}`);